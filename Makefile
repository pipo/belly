OBJECTS = main.o option.o package.o settings.o strlist.o untar.o

all: $(OBJECTS)
	gcc -g $(OBJECTS) -o belly -lsqlite3 -lcurl -larchive

%.o: %.c
	gcc -g -c $< -o $@

clean:
	rm -f $(OBJECTS) belly

install:
	install -dm755 $(DESTDIR)/usr/bin
	install -m755 belly $(DESTDIR)/usr/bin
