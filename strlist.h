#ifndef STRLIST_H
#define STRLIST_H

typedef struct package {
	char *name;
	struct package *next;
} package;


package *str_list_new(const char *name);
void str_list_free(package *p);
void str_list_add(package *p, const char *name);
void str_list_add_list(package *from, package *to);
package *str_list_new_cpy(package *p);
void str_list_print(package *p);
int str_list_size(package *p);
int str_list_contains(package *p, char *name);

#endif
