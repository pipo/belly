#include "strlist.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

package *str_list_new(const char *name) {
	char *nname = (char *)malloc(sizeof(char) * (strlen(name)+2));
	strncpy(nname, name, strlen(name));
	nname[strlen(name)] = '\0';
	package *p = (package *)malloc(sizeof(package));
	p->name = nname;
	p->next = NULL;
	return p;
}

void str_list_free(package *p) {
	if(p == NULL)
		return;

	str_list_free(p->next);
	free(p->name);
	free(p);
}

void str_list_add(package *p, const char *name) {
	if(strcmp(p->name, name) == 0)
		return;

	if(p->next == NULL) {
		p->next = str_list_new(name);
		return;
	}

	str_list_add(p->next, name);
}

void str_list_add_list(package *from, package *to) {
	while(from != NULL) {
		str_list_add(to, from->name);
		from = from->next;
	}
}

package *str_list_new_cpy(package *p) {
	package *new_p = NULL;
	if(p != NULL) {
		new_p = str_list_new(p->name);
		p = p->next;
	}
	package *temp = new_p;
	while(p != NULL) {
		temp->next = str_list_new(p->name);
		temp = temp->next;
		p = p->next;
	}
	return new_p;
}

void str_list_print(package *p) {
	package *temp = p;
	while(temp != NULL) {
		printf("%s ", temp->name);
		temp = temp->next;
	}
	printf("\n");
}

int str_list_size(package *p) {
	if(p == NULL)
		return 0;

	return 1 + str_list_size(p->next);
}

int str_list_contains(package *p, char *name) {
	if(p == NULL)
		return 0;
	
	if(strcmp(p->name, name) == 0)
		return 1;
	
	return str_list_contains(p->next, name);
}
