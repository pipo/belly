/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "option.h"
#include "package.h"

#include <sqlite3.h>

#define VERSION "0.1"

void actual_install(package *list, option *o);

void printUsage(char *name) {
	printf(
"Usage: %s [options] command <package> [ <package>... ]\n"
"\n"
"Commands:\n"
"   install: installs or updates the packages\n"
"   remove, uninstall: removes the packages\n"
"   describe: prints package description\n"
"   refresh, update: gets latest package list\n"
"   upgrade: refresh and install new versions of packages\n"
"\n"
"Options:\n"
"   -d, --dry-run: only print what would be done\n"
"   -v, --verbose: prints more output\n"
"   -h, --help: prints this help and exits\n"
"   -V, --version: prints version number and exits\n"
"   -t, --target: install to / remove from another directory\n"
	, name);
}

void printVersion() {
	printf("Version %s\n", VERSION);
}

void install(option *o);
void uninstall(option *o);
void refresh(option *o);
void upgrade(option *o);
void describe(option *o);

int main(int argc, char *argv[]) {
	if(argc == 0) {
		return 1;
	}
	if(argc < 2) {
		printUsage(argv[0]);
		return 1;
	}
	option *o = readOptions(argc, argv);
	if(o->opt & opt_version)
		printVersion();
	if(o->opt & opt_help)
		printUsage(argv[0]);
	if(o->opt & opt_continue) {
		if(o->cmd == cmd_none) {
			printf("No command specified!\n");
			printUsage(argv[0]);
			option_free(o);
			return 1;
		}
		if(library_init() != SQLITE_OK) {
			printf("Could not open package database!\n");
			library_uninit();
			option_free(o);
			return 1;
		}
		
		printf("Starting operations.\n");
		if(o->cmd == cmd_install) {
			install(o);
		} else if(o->cmd == cmd_remove) {
			uninstall(o);
		} else if(o->cmd == cmd_refresh) {
			refresh(o);
		} else if(o->cmd == cmd_upgrade) {
			upgrade(o);
		} else if(o->cmd == cmd_describe) {
			describe(o);
		}

		library_uninit();
	}
	option_free(o);
	return 0;
}

void install(option *o) {
	package *dependencies = str_list_new_cpy(o->pkgs);
	package *temp = dependencies;
	package *to_install = NULL;
	while(temp != NULL) {
		char *latest = package_latest_version(temp->name);
		char *current = package_current_version(temp->name);
		if(latest == NULL) {
			printf("%s not found in repo, skipping it.\n", temp->name);
			if(current != NULL)
				free(current);
			temp = temp->next;
			continue;
		}
		if(current == NULL || strcmp(latest, current) != 0) {
			package *deps = package_dependencies(temp->name);
			str_list_add_list(deps, dependencies);
			str_list_free(deps);
			if(to_install == NULL) {
				to_install = str_list_new(temp->name);
			} else {
				str_list_add(to_install, temp->name);
			}
		}
		free(latest);
		free(current);
		temp = temp->next;
	}
	str_list_free(dependencies);

	if(to_install == NULL) {
		printf("Packages are already installed and up to date.\n");
		return;
	}

	printf("Those packages will be installed / updated:\n");
	str_list_print(to_install);
	printf("Downloading package archives...\n");
	temp = to_install;
	int ctn = 1;
	while(temp != NULL) {
		if(!package_download(temp->name)) {
			printf("Could not download %s!\n", temp->name);
			ctn = 0;
			break;
		}
		temp = temp->next;
	}
	if(ctn) {
		printf("Verifying package content...\n");
		temp = to_install;
		while(temp != NULL) {
			if(!package_verify(temp->name, o->target)) {
				ctn = 0;
				break;
			}
			temp = temp->next;
		}
	}
	if(ctn) {
		printf("Installing packages...\n");
		actual_install(to_install, o);
	}
	str_list_free(to_install);
}

/* installs packages in revers order, so dependencies are installed first */
void actual_install(package *list, option *o) {
	if(list == NULL)
		return;
	actual_install(list->next, o);
	package_install(list->name, o->target);
}

void uninstall(option *o) {
	package *dependencies = str_list_new_cpy(o->pkgs);
	package *temp = dependencies;
	package *to_remove = NULL;
	while(temp != NULL) {
		char *latest = package_latest_version(temp->name);
		char *current = package_current_version(temp->name);
		if(current == NULL) {
			printf("%s not installed, skipping it.\n", temp->name);
			if(latest != NULL)
				free(latest);
			temp = temp->next;
			continue;
		} else {
			package *deps = package_dependants(temp->name);
			str_list_add_list(deps, dependencies);
			str_list_free(deps);
			if(to_remove == NULL) {
				to_remove = str_list_new(temp->name);
			} else {
				str_list_add(to_remove, temp->name);
			}
		}
		free(latest);
		free(current);
		temp = temp->next;
	}
	str_list_free(dependencies);
	printf("Removing ");
	str_list_print(to_remove);

	temp = to_remove;
	while(temp != NULL) {
		package_remove(temp->name, o->target);
		temp = temp->next;
	}

	str_list_free(to_remove);
}

void refresh(option *o) {
	printf("TODO\n");
}

void upgrade(option *o) {
	str_list_free(o->pkgs);
	o->pkgs = package_installed_list();
	install(o);
}

void describe(option *o) {
	int i = 0;
	package *pkgs = o->pkgs;
	while(pkgs != NULL) {
		if(!package_exists(pkgs->name))
			printf("%s is not in the database.\n", pkgs->name);
		else
			package_describe(pkgs->name);
		printf("\n");
		pkgs = pkgs->next;
	}
}
