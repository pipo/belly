/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "package.h"

#include "settings.h"
#include "strlist.h"
#include "untar.h"

#include <sys/ioctl.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sqlite3.h>
#include <archive.h>
#include <archive_entry.h>
#include <curl/curl.h>

sqlite3 *db_repo;
sqlite3 *db_system;

extern settings_t *settings;

int library_init() {
	settings_init();
	curl_global_init(CURL_GLOBAL_ALL);
	int rc = sqlite3_open("/pipo/repo.db", &db_repo);
	return (rc == SQLITE_OK)? sqlite3_open("/pipo/system.db", &db_system): rc;
}

int library_uninit() {
	sqlite3_close(db_repo);
	sqlite3_close(db_system);
	curl_global_cleanup();
	settings_uninit();
}

int package_exists(char *pkg) {
	sqlite3_stmt *res;
	sqlite3_prepare(db_repo, "select name from package where name = ?",
			-1, &res, 0);
	sqlite3_bind_text(res, 1, pkg, strlen(pkg), SQLITE_TRANSIENT);
	int rc = sqlite3_step(res);
	int ans = 0;
	if (rc == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res, 0);
		ans = (strlen(name) > 0);
	}
	sqlite3_finalize(res);
	return ans;
}

void package_describe(char *pkg) {
	sqlite3_stmt *res, *res2, *res3;
	sqlite3_prepare(db_repo, 
			"select id,description,version from package where name = ?",
			-1, &res, 0);
	sqlite3_bind_text(res, 1, pkg, strlen(pkg), SQLITE_TRANSIENT);
	int rc = sqlite3_step(res);
	int id = 0;
	if (rc == SQLITE_ROW) {
		id = sqlite3_column_int(res, 0);
		const char *description = sqlite3_column_text(res, 1);
		const char *version = sqlite3_column_text(res, 2);
		printf("%s-%s:\n", pkg, version);
		printf("ID: %d\n", id);
		if(description != NULL)
			printf("%s\n", description);
	}
	sqlite3_finalize(res);
	printf("Depends on:\n");
	sqlite3_prepare(db_repo, "select name from package join dependency d on"
			" package.id = d.depends where d.dependant = ?",
			-1, &res2, 0);
	sqlite3_bind_int(res2, 1, id);
	while(sqlite3_step(res2) == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res2, 0);
		printf("  %s\n", name);
	}
	sqlite3_finalize(res2);
	printf("Needed by:\n");
	sqlite3_prepare(db_repo, "select name from package join dependency d on"
			" package.id = d.dependant where d.depends = ?",
			-1, &res3, 0);
	sqlite3_bind_int(res3, 1, id);
	while(sqlite3_step(res3) == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res3, 0);
		printf("  %s\n", name);
	}
	sqlite3_finalize(res3);
}

package *package_installed_list() {
	package *p = NULL;
	sqlite3_stmt *res;
	sqlite3_prepare(db_system, "select name from package", -1, &res, 0);
	while(sqlite3_step(res) == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res, 0);
		if(p == NULL) {
			p = str_list_new(name);
		} else {
			str_list_add(p, name);
		}
	}
	sqlite3_finalize(res);
	return p;
}

package *package_dependencies(char *name) {
	package *p = NULL;
	sqlite3_stmt *res;
	sqlite3_prepare(db_repo, "select package.name from package join"
			" dependency d on"
			" package.id = d.depends join package p on p.id = d.dependant"
			" where p.name = ?",
			-1, &res, 0);
	sqlite3_bind_text(res, 1, name, -1, SQLITE_TRANSIENT);
	while(sqlite3_step(res) == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res, 0);
		if(p == NULL) {
			p = str_list_new(name);
		} else {
			str_list_add(p, name);
		}
	}
	sqlite3_finalize(res);
	return p;
}

package *package_dependants(char *name) {
	package *p = NULL;
	sqlite3_stmt *res;
	sqlite3_prepare(db_repo, "select package.name from package join"
			" dependency d on"
			" package.id = d.dependant join package p on p.id = d.depends"
			" where p.name = ?",
			-1, &res, 0);
	sqlite3_bind_text(res, 1, name, -1, SQLITE_TRANSIENT);
	while(sqlite3_step(res) == SQLITE_ROW) {
		const char *name = sqlite3_column_text(res, 0);
		if(p == NULL) {
			p = str_list_new(name);
		} else {
			str_list_add(p, name);
		}
	}
	sqlite3_finalize(res);
	return p;
}

char *package_version(char *name, sqlite3 *db) {
	if(name == NULL)
		return NULL;
	sqlite3_stmt *res;
	sqlite3_prepare(db, 
			"select version from package where name = ?",
			-1, &res, 0);
	sqlite3_bind_text(res, 1, name, strlen(name), SQLITE_TRANSIENT);
	int rc = sqlite3_step(res);
	const char *version = NULL;
	if (rc == SQLITE_ROW) {
		version = sqlite3_column_text(res, 0);
	}
	if(version == NULL)
		return NULL;
	char *ans = (char *)malloc(sizeof(char) * (strlen(version) + 1));
	strncpy(ans, version, strlen(version));
	ans[strlen(version)] = '\0';
	sqlite3_finalize(res);
	return ans;
}

char *package_current_version(char *name) {
	return package_version(name, db_system);
}
char *package_latest_version(char *name) {
	return package_version(name, db_repo);
}

int package_progress(void* ptr, double TotalToDownload,
		double NowDownloaded, double TotalToUpload, double NowUploaded) {
	if (TotalToDownload <= 0.0) {
        return 0;
    }
	
    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
	int width = w.ws_col;
	if(width < 10)
		return 0;
	
	double percent = 100 * NowDownloaded / TotalToDownload;
	int i;
	printf("%.39s ", ptr);
	for(i=strlen(ptr) + 2; i<40; i++)
		printf(" ");
	printf("[");
	for(i=40;i<width-6;i++) {
		printf("%c", ((100 * (double)(i-40) / (double)(width-46)) < percent)? '#': ' ');
	}
	printf("] %3.0f%%\r", percent);
	fflush(stdout);
	return 0;
}

char *package_repo_path(char *name, char *version, int partial) {
	int fullname_size = strlen(name) + 1 + strlen(version);
	int path_size = settings->repo_len + 1 + fullname_size + 7;
	char *path = (char *)malloc(sizeof(char) * (path_size + 1));
	snprintf(path, path_size+1, partial? "%s/%s-%s.partit": "%s/%s-%s.tar.gz",
			settings->repo, name, version);
	path[path_size] = '\0';
	return path;
}

int package_download(char *name) {
	char *version = package_latest_version(name);
	if(version == NULL)
		return 0;
	int fullname_size = strlen(name) + 1 + strlen(version);
	int url_size = settings->url_len + 1 + fullname_size + 7;
	char *url = (char *)malloc(sizeof(char) * (url_size + 1));
	char *fullname = (char *)malloc(sizeof(char) * (fullname_size + 1));
	char *path = package_repo_path(name, version, 1);
	char *real_path = package_repo_path(name, version, 0);
	if(access(real_path, F_OK) == 0) {
		free(version);
		free(url);
		free(path);
		free(real_path);
		free(fullname);
		return 1;
	}
	snprintf(url, url_size+1, "%s/%s-%s.tar.gz", settings->url, name, version);
	snprintf(fullname, fullname_size+1, "%s-%s", name, version);
	url[url_size] = '\0';
	FILE *file = fopen(path, "ab");
	if(file == NULL) {
		printf("Could not open file for writing!\n");
		free(url);
		free(fullname);
		free(path);
		free(version);
		return 0;
	}
	
	char errbuf[CURL_ERROR_SIZE];
	memset(errbuf, 0, CURL_ERROR_SIZE);

	int file_size = 0;
	if(access(path, F_OK) == 0) {
		fseek(file, 0, SEEK_END);
		file_size = ftell(file);
	}

	char range[1024];
	snprintf(range, 1023, "%d-", file_size);
	
	CURL *curl_handle = curl_easy_init();
	curl_easy_setopt(curl_handle, CURLOPT_URL, url);
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, file);
	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0);
	curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, package_progress);
	curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, fullname);
	curl_easy_setopt(curl_handle, CURLOPT_ERRORBUFFER, errbuf);
	curl_easy_setopt(curl_handle, CURLOPT_FAILONERROR, 1);
	curl_easy_setopt(curl_handle, CURLOPT_RANGE, range);
	int success = curl_easy_perform(curl_handle);
	printf("\n");
	if(rename(path, real_path) != 0) {
		success = !CURLE_OK;
		printf("Error moving part file to actual archive.\n");
	}

	fclose(file);
	curl_easy_cleanup(curl_handle);
	free(url);
	free(fullname);
	free(path);
	free(version);
	
	if(success != CURLE_OK && strlen(errbuf) != 0) {
		printf("Download error: %s\n", errbuf);
	}
	return success == CURLE_OK;
}

package *content_list_new_archive(char *path, const char *target) {
	struct archive *a;
	struct archive_entry *entry;
	int r;
	package *p = NULL;

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);

	r = archive_read_open_filename(a, path, 10240);
	if(r != ARCHIVE_OK) {
		archive_read_free(a);
		return NULL;
	}

	while (archive_read_next_header(a, &entry) == ARCHIVE_OK) {
		const char *data = archive_entry_pathname(entry);
		char *actual_data = (char *)malloc(sizeof(char) * (strlen(data) +
				strlen(target) + 2));
		sprintf(actual_data, "%s/%s", target, data);
		if(p == NULL) {
			p = str_list_new(actual_data);
		} else {
			str_list_add(p, actual_data);
		}
		archive_read_data_skip(a);
		free(actual_data);
	}
	r = archive_read_free(a);
	return p;
}

int package_verify_update(char *name, char *current, const char *target) {
	char *version = package_latest_version(name);
	char *path_new = package_repo_path(name, version, 0);
	char *path_old = package_repo_path(name, current, 0);
	package *list_new = content_list_new_archive(path_new, target);
	package *list_old = content_list_new_archive(path_old, target);
	free(path_new);
	free(path_old);

	int length = str_list_size(list_new);
	int i = 0;
	package *temp = list_new;
	while(temp != NULL) {
		if(!str_list_contains(list_old, temp->name)
					&& access(temp->name, F_OK) == 0) {
			printf("\n%s is already present on file system!"
					" This is a conflict!\n", temp->name);
			free(version);
			str_list_free(list_old);
			str_list_free(list_new);
			return 0;
		}
		i++;
		package_progress(name, (double)length, (double)i, 0, 0);
		temp = temp->next;
	}
	printf("\n");
	
	free(version);
	str_list_free(list_new);
	str_list_free(list_old);
	return 1;
}

int package_verify_install(char *name, const char *target) {
	char *version = package_latest_version(name);
	char *path = package_repo_path(name, version, 0);
	package *list = content_list_new_archive(path, target);
	if(list == NULL) {
		printf("Could not open package!\n");
		return 0;
	}

	int length = str_list_size(list);
	int i = 0;
	package *temp = list;
	while(temp != NULL) {
		if(access(temp->name, F_OK) == 0) {
			printf("\n%s is already present on file system!"
					" This is a conflict!\n", temp->name);
			str_list_free(list);
			free(version);
			return 0;
		}
		i++;
		package_progress(name, length, i, 0, 0);
		temp = temp->next;
	}
	printf("\n");
	str_list_free(list);
	free(version);
	return 1;
}

int package_verify(char *name, const char *target) {
	char *current = package_current_version(name);
	if(current != NULL) {
		int ans = package_verify_update(name, current, target);
		free(current);
		return ans;
	} else {
		return package_verify_install(name, target);
	}
}

int package_install_remove_old(char *name, char *new, char *old, const char *target) {
	chdir(target);
	char *path_new = package_repo_path(name, new, 0);
	char *path_old = package_repo_path(name, old, 0);
	package *list_new = content_list_new_archive(path_new, target);
	package *list_old = content_list_new_archive(path_old, target);
	package *temp = list_old;
	
	int i = 0;
	int length = str_list_size(list_old);
	while(temp != NULL) {
		if(!str_list_contains(list_new, temp->name)) {
			unlink(temp->name);
		}
		i++;
		package_progress(name, length, i, 0, 0);
		temp = temp->next;
	}
	printf("\n");
	
	free(path_new);
	free(path_old);
	str_list_free(list_new);
	str_list_free(list_old);
}

int package_get_id(char *name) {
	sqlite3_stmt *res;
	sqlite3_prepare(db_system, "select id from package where name = ?", -1, 
			&res, 0);
	sqlite3_bind_text(res, 1, name, -1, SQLITE_TRANSIENT);
	int id = 0;
	int rc = sqlite3_step(res);
	if (rc == SQLITE_ROW) {
		id = sqlite3_column_int(res, 0);
	}

	sqlite3_finalize(res);
	return id;
}

void package_db_insert_dep(char *name) {
	package *deps = package_dependencies(name);
	package *temp = deps;
	sqlite3_stmt *res;
	int id = package_get_id(name);
	while(temp != NULL) {
		sqlite3_prepare(db_system, "insert into dependency(dependant, depends)"
			" values( ? , ? )", -1, &res, 0);
		sqlite3_bind_int(res, 1, id);
		sqlite3_bind_int(res, 2, package_get_id(temp->name));
		sqlite3_step(res);
		sqlite3_finalize(res);
		temp = temp->next;
	}
	str_list_free(deps);
}

void package_db_insert(char *name, char *version) {
	sqlite3_stmt *res;
	sqlite3_prepare(db_system, "insert into package(name, version)"
			" values( ? , ? )", -1, &res, 0);
	sqlite3_bind_text(res, 1, name, -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(res, 2, version, -1, SQLITE_TRANSIENT);
	sqlite3_step(res);
	sqlite3_finalize(res);
	package_db_insert_dep(name);
}

void package_db_update(char *name, char *version) {
	sqlite3_stmt *res;
	sqlite3_prepare(db_system, "update package set version = ?"
			" where name = ?", -1, &res, 0);
	sqlite3_bind_text(res, 1, version, -1, SQLITE_TRANSIENT);
	sqlite3_bind_text(res, 2, name, -1, SQLITE_TRANSIENT);
	sqlite3_step(res);
	sqlite3_finalize(res);

	sqlite3_prepare(db_system, "delete from dependency d"
		" join package p on p.id = d.dependant"
		" where p.name = ?", -1, &res, 0);
	sqlite3_bind_text(res, 1, name, -1, SQLITE_TRANSIENT);
	sqlite3_step(res);
	sqlite3_finalize(res);

	package_db_insert_dep(name);
}

int package_install(char *name, const char *target) {
	char *previous = package_current_version(name);
	char *version = package_latest_version(name);
	char *path = package_repo_path(name, version, 0);
	int status = extract(name, path, target);
	if(previous != NULL) {
		package_install_remove_old(name, version, previous, target);
		package_db_update(name, version);
	} else {
		package_db_insert(name, version);
	}
	remove_recipe(target);
	post_install(target);
	free(path);
	free(previous);
	free(version);
	return status;
}

int package_remove(char *name, const char *target) {
	chdir(target);
	char *version = package_current_version(name);
	char *path = package_repo_path(name, version, 0);
	free(version);
	package *files = content_list_new_archive(path, target);
	free(path);
	package *temp = files;
	int i = 0;
	int length = str_list_size(files);
	while(temp != NULL) {
		unlink(temp->name);
		temp = temp->next;
		i++;
		package_progress(name, (double)length, (double)i, 0, 0);
	}
	printf("\n");
	
	sqlite3_stmt *res;
	int id = package_get_id(name);

	sqlite3_prepare(db_system, "delete from dependency"
			" where dependant = ?", -1, &res, 0);
	sqlite3_bind_int(res, 1, id);
	sqlite3_step(res);
	sqlite3_finalize(res);

	sqlite3_prepare(db_system, "delete from package"
			" where id = ?", -1, &res, 0);
	sqlite3_bind_int(res, 1, id);
	sqlite3_step(res);
	sqlite3_finalize(res);
	
	str_list_free(files);
	return 1;
}

void remove_recipe(const char *target) {
	char *recipe = (char *)malloc(sizeof(char) * (strlen(target) + 8));
	sprintf(recipe, "%s/recipe", target);
	unlink(recipe);
	free(recipe);
}

void post_install(const char *target) {
	char *file = (char *)malloc(sizeof(char) * (strlen(target) + 14));
	sprintf(file, "%s/post-install", target);
	if(access(file, F_OK) != 0) {
		free(file);
		return;
	}
	
	int pid = fork();
	if(pid == 0) {
		execlp("chroot", "chroot", target, "/bin/bash", "/post-install", NULL);
	} else {
		int status;
		wait(&status);
	}
	unlink(file);
	free(file);
}
