/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "option.h"

option *readOptions(int argc, char *argv[]) {
	option *opt = (option *)malloc(sizeof(option));
	opt->cmd = cmd_none;
	opt->opt = opt_continue;
	opt->pkgs = NULL;
	opt->target = "/";
	int i;
	for(i=1; i<argc; i++) {
		if(strcmp("-V", argv[i]) == 0
			|| strcmp("--version", argv[i]) == 0) {
			opt->opt |= opt_version;
			opt->opt &= ~opt_continue;
		} else if(strcmp("-h", argv[i]) == 0
					|| strcmp("--help", argv[i]) == 0) {
			opt->opt |= opt_help;
			opt->opt &= ~opt_continue;
		} else if(strcmp("-d", argv[i]) == 0
					|| strcmp("--dryrun", argv[i]) == 0) {
			opt->opt |= opt_dry_run;
			opt->opt &= ~opt_continue;
		} else if(strcmp("--target", argv[i]) == 0
					|| strcmp("-t", argv[i]) == 0) {
			if(i+1 == argc) {
				printf("No target argument!\n");
				opt->opt &= ~opt_continue;
			} else {
				i++;
				opt->target = argv[i];
			}
		} else if(strcmp("--install", argv[i]) == 0
				|| strcmp("--eat", argv[i]) == 0) {
			opt->cmd = cmd_install;
		} else if(strcmp("--remove", argv[i]) == 0
					|| strcmp("--uninstall", argv[i]) == 0
					|| strcmp("--digest", argv[i]) == 0) {
			opt->cmd = cmd_remove;
		} else if(strcmp("--refresh", argv[i]) == 0) {
			opt->cmd = cmd_refresh;
		} else if(strcmp("--upgrade", argv[i]) == 0
				|| strcmp("--diet", argv[i]) == 0) {
			opt->cmd = cmd_upgrade;
		} else if(strcmp("--describe", argv[i]) == 0) {
			opt->cmd = cmd_describe;
		} else if(argv[i][0] == '-') {
			printf("Unknown option %s", argv[i]);
			opt->opt |= opt_help;
			opt->opt &= ~opt_continue;
		} else {
			package *new_pkg = str_list_new(argv[i]);
			if(opt->pkgs == NULL) {
				opt->pkgs = new_pkg;
			} else {
				str_list_add_list(new_pkg, opt->pkgs);
				str_list_free(new_pkg);
			}
		}
	}
	return opt;
}

void option_free(option *o) {
	str_list_free(o->pkgs);
	free(o);
}
