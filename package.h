/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PACKAGE_H
#define PACKAGE_H

#include <stdlib.h>
#include "strlist.h"


int library_init();
int library_uninit();
int package_exists(char *name);
void package_describe(char *name);
package *package_dependencies(char *name);
package *package_dependants(char *name);
char *package_latest_version(char *name);
char *package_current_version(char *name);
int package_install(char *name, const char *target);
int package_remove(char *name, const char *target);
char *package_repo_path(char *name, char *version, int partial);
int package_download(char *name);
int package_verify(char *name, const char *target);
package *package_installed_list();
int package_progress(void* ptr, double TotalToDownload,
		double NowDownloaded, double TotalToUpload, double NowUploaded);
void post_install(const char *target);
void remove_recipe(const char *target);

size_t package_write_data(void *buffer, size_t size, size_t nmemb, void *userp);


#endif
