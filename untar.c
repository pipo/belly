/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "untar.h"

#include "package.h"

#include <sys/types.h>

#include <sys/stat.h>
#include <libgen.h>

#include <archive.h>
#include <archive_entry.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int copy_data(struct archive *, struct archive *);
int tar_length(const char *filename);
void create_directory(char *filename);

int extract(char *name, const char *filename, const char *target) {
	chdir(target);
	struct archive *a;
	struct archive *ext;
	struct archive_entry *entry;
	int r;
	int length = tar_length(filename);

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);
	ext = archive_write_disk_new();
	int flags = ARCHIVE_EXTRACT_TIME;
		flags |= ARCHIVE_EXTRACT_PERM;
		flags |= ARCHIVE_EXTRACT_ACL;
		flags |= ARCHIVE_EXTRACT_FFLAGS;
	archive_write_disk_set_options(ext, flags);
	archive_write_disk_set_standard_lookup(ext);
	if (filename != NULL && strcmp(filename, "-") == 0)
		filename = NULL;
	if ((r = archive_read_open_filename(a, filename, 10240)))
		return 0;
	int i = 0;
	for (;;) {
		r = archive_read_next_header(a, &entry);
		if (r == ARCHIVE_EOF)
			break;
		if (r != ARCHIVE_OK)
			return 0;
		const char *data = archive_entry_pathname(entry);
		char *actual_data = (char *)malloc(sizeof(char) * (strlen(data) +
				strlen(target) + 2));
		sprintf(actual_data, "%s/%s", target, data);
		if(strncmp(data, "etc", 3) == 0 && access(actual_data, F_OK) == 0) {
			i++;
			package_progress(name, length, i, 0, 0);
			continue;
		}
		create_directory(dirname(actual_data));
		r = archive_write_header(ext, entry);
		if (r != ARCHIVE_OK) {
			i++;
			package_progress(name, length, i, 0, 0);
			continue;
		}
		copy_data(a, ext);
		r = archive_write_finish_entry(ext);
		if (r != ARCHIVE_OK)
			return 0;
		i++;
		package_progress(name, length, i, 0, 0);
	}
	printf("\n");
	archive_read_close(a);
	archive_read_free(a);
	return 1;
}

int copy_data(struct archive *ar, struct archive *aw) {
	int r;
	const void *buff;
	size_t size;
#if ARCHIVE_VERSION_NUMBER >= 3000000
	int64_t offset;
#else
	off_t offset;
#endif

	for (;;) {
		r = archive_read_data_block(ar, &buff, &size, &offset);
		if (r == ARCHIVE_EOF)
			return (ARCHIVE_OK);
		if (r != ARCHIVE_OK)
			return (r);
		r = archive_write_data_block(aw, buff, size, offset);
		if (r != ARCHIVE_OK) {
			return (r);
		}
	}
}

int tar_length(const char *filename) {
	struct archive *a;
	struct archive_entry *entry;
	int r;
	int i = 0;

	a = archive_read_new();
	archive_read_support_filter_all(a);
	archive_read_support_format_all(a);

	r = archive_read_open_filename(a, filename, 10240);
	if(r != ARCHIVE_OK) {
		archive_read_free(a);
		return i;
	}

	while (archive_read_next_header(a, &entry) == ARCHIVE_OK) {
		const char *data = archive_entry_pathname(entry);
		i++;
		archive_read_data_skip(a);
	}
	r = archive_read_free(a);
	return i;
}

void create_directory(char *filename) {
	if(strcmp(filename, "/") == 0 || strcmp(filename, "") == 0
			|| strcmp(filename, "//") == 0)
		return;
	create_directory(dirname(filename));
	mkdir(filename, S_IRWXU || S_IRGRP || S_IXGRP || S_IROTH || S_IXOTH);
}
