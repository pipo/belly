/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "settings.h"

#include <stdlib.h>
#include <string.h>

settings_t *settings;

void settings_init() {
	settings = (settings_t *)malloc(sizeof(settings_t));
	settings->url = "http://repo.pipo-distro.tk/repo/x86_64";
	settings->repo = "/pipo/repo";
	settings->repo_len = strlen(settings->repo);
	settings->url_len = strlen(settings->url);
}

void settings_uninit() {
	//free(settings->url);
	//free(settings->repo);
	free(settings);
}
