/*
 * Copyright 2015 Julien Lepiller <julien@lepiller.eu>
 *
 * This file is part of Pipo Belly.
 *
 * Belly is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Belly is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Belly.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "package.h"

typedef enum OPT {
	opt_continue = 1,
	opt_help = 2,
	opt_version = 4,
	opt_dry_run = 8
} OPT;

typedef enum command {
	cmd_none = 0,
	cmd_install,
	cmd_remove,
	cmd_refresh,
	cmd_upgrade,
	cmd_describe
} command;

typedef struct {
	OPT opt;
	command cmd;
	package *pkgs;
	char *target;
} option;

option *readOptions(int argc, char *argv[]);
void option_free(option *o);
